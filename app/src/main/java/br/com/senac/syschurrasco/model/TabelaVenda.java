package br.com.senac.syschurrasco.model;

import android.content.ClipData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TabelaVenda {

    private Date vigenciaInicio;
    private Date vigenciaFinal;
    private List<ItemTabelaVenda> itens = new ArrayList<>();

    public TabelaVenda() {
    }

    public TabelaVenda(Date vigenciaInicio, Date vigenciaFinal) {
        this.vigenciaInicio = vigenciaInicio;
        this.vigenciaFinal = vigenciaFinal;

    }

    public Date getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(Date vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public Date getVigenciaFinal() {
        return vigenciaFinal;
    }

    public void setVigenciaFinal(Date vigenciaFinal) {
        this.vigenciaFinal = vigenciaFinal;
    }

    public List<ItemTabelaVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemTabelaVenda> itens) {
        this.itens = itens;
    }

    public void add(ItemTabelaVenda item) {
        this.itens.add(item);
    }

    public void remove(ItemTabelaVenda item) {
        this.itens.remove(item);
    }
}
