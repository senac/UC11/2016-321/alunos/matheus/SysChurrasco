package br.com.senac.syschurrasco.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.ItemTabelaVenda;
import br.com.senac.syschurrasco.model.Produto;
import br.com.senac.syschurrasco.model.TabelaVenda;

public class ListaTabelaVendaActivity extends AppCompatActivity {

    private TabelaVenda TabelaVenda = new TabelaVenda();

    private ListView listViewTabelaVenda;

    public ListaTabelaVendaActivity(){
        super();
        initTabelaVenda();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tabela_venda);

        int layout = android.R.layout.simple_list_item_multiple_choice;

        listViewTabelaVenda = (ListView) findViewById(R.id.lista);

        ArrayAdapter<ItemTabelaVenda> adapter = new ArrayAdapter<ItemTabelaVenda>(this, layout, TabelaVenda.getItens());


    }

    private void initTabelaVenda() {



    }
}
