package br.com.senac.syschurrasco.model;


import java.text.NumberFormat;

public class ItemTabelaVenda {

    private double preco;
    private Produto produto;
    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    public ItemTabelaVenda() {
    }

    public ItemTabelaVenda(double preco, Produto produto) {
        this.preco = preco;
        this.produto = produto;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
}
